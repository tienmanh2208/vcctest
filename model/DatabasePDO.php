<?php
/**
 * Created by PhpStorm.
 * User: tienmanh2208
 * Date: 21/03/2019
 * Time: 13:51
 */

$rootPath = $_SERVER['DOCUMENT_ROOT'];

require_once $rootPath . '/project2/config.php';

class DatabasePDO
{
    private $conn;

    public function __construct()
    {
        global $servername;
        global $username;
        global $password;
        global $dbname;

        $dsn = "mysql:host=$servername;dbname=$dbname";

        try {
            // create a PDO connection with the configuration data
            $this->conn = new PDO($dsn, $username, $password);

            // display a message if connected to database successfully
            if ($this->conn) {
                echo "<script> console.log('Connected to the $dbname database successfully!')</script>";
            }
        } catch (PDOException $e) {
            // report error message
            echo $e->getMessage();
        }
    }

    /**
     * @param $strTablename
     * @param $value array
     * @return bool
     */
    public function createNewRecord($strTablename, $arrValue)
    {
        $lengthValue = sizeof($arrValue);

        if ($lengthValue == 0 || !isset($this->conn)) return false;
        else {
            $valueStmt = '';
            for ($i = 0; $i < $lengthValue; ++$i) $valueStmt .= '?, ';
            $valueStmt = rtrim($valueStmt, ', ');

            $query = "INSERT INTO " . $strTablename . " VALUES (" . $valueStmt . ")";

            $stmt = $this->conn->prepare($query);

            return $stmt->execute($arrValue);
        }
    }

    /**
     * @param $strTablename
     * @param $arrField array
     * @param $arrFilter array
     * @return bool|false|PDOStatement
     */
    public function getInfo($strTablename, $arrField, $arrFilter)
    {
        $strField = isset($arrField) ? ((sizeof($arrField) == 0) ? "*" : implode(",", $arrField)) : "*";
        $whereStmt = '';

        if (isset($arrFilter) && sizeof($arrFilter) != 0) { // if contain where conditions
            // Generate where statement
            $whereStmt .= " WHERE ";
            foreach ($arrFilter as $name => $value) {
                $whereStmt .= $name . '=? and ';
            }

            $whereStmt = rtrim($whereStmt, ' and');
        }

        $query = "SELECT DISTINCT " . $strField . " FROM " . $strTablename . $whereStmt;

        $stmt = $this->conn->prepare($query);

        return $stmt->execute(array_values($arrFilter));
    }

    /**
     * @param $strTablename
     * @param $arrValuesFilter
     * @param $arrUpdatedValues
     * @return bool
     */
    public function updateInfo($strTablename, $arrValuesFilter, $arrUpdatedValues)
    {
        if (!isset($arrValuesFilter) || sizeof($arrValuesFilter) == 0 || !isset($arrUpdatedValues) || sizeof($arrUpdatedValues) == 0) return false;

        $setStmt = '';
        $whereStmt = '';

        // Generate set statement
        foreach ($arrUpdatedValues as $name => $value) $setStmt .= $name . ' = ? , ';
        $setStmt = rtrim($setStmt, ', ');

        // Generate where statement
        foreach ($arrValuesFilter as $name => $value) $whereStmt .= $name . ' = ? and ';
        $whereStmt = rtrim($whereStmt, 'and ');

        $query = 'UPDATE ' . $strTablename . ' SET ' . $setStmt . ' WHERE ' . $whereStmt;

        $stmt = $this->conn->prepare($query);

        return $stmt->execute(array_merge(array_values($arrUpdatedValues), array_values($arrValuesFilter)));
    }

    /**
     * @param $strTablename
     * @param $name array
     * @param $value array
     * @return bool
     */
    public function deleteRecord($strTablename, $arrValuesFilter)
    {
        if(!isset($arrValuesFilter) || sizeof($arrValuesFilter) == 0) return false;
        $whereStmt = '';

        // Generate where statement
        foreach ($arrValuesFilter as $name => $value) $whereStmt .= $name . ' = ? and ';
        $whereStmt = rtrim($whereStmt, 'and ');

        $query = "DELETE FROM " . $strTablename . " WHERE " . $whereStmt;

        $stmt = $this->conn->prepare($query);
        return $stmt->execute(array_values($arrValuesFilter));
    }

    /**
     * @param $selectStmt
     * @param $fromStmt
     * @param $whereStmt
     * @param $groupbyStmt
     * @param $havingStmt
     * @param $orderbyStmt
     * @return bool
     */
    public function customQuerySelect($selectStmt, $fromStmt, $whereStmt, $groupbyStmt, $havingStmt, $orderbyStmt)
    {
        $query = '';

        if (!isset($selectStmt) || !isset($fromStmt)) return false;
        else {
            $query .= 'SELECT DISTINCT' . $selectStmt . ' FROM ' . $fromStmt;

            if (isset($whereStmt)) {
                $query .= ' WHERE ' . $whereStmt;

                if (isset($groupbyStmt)) {
                    $query .= ' GROUP BY ' . $groupbyStmt;

                    if (isset($havingStmt)) {
                        $query .= ' HAVING ' . $havingStmt;

                        if (isset($orderbyStmt)) {
                            $query .= ' ORDER BY ' . $orderbyStmt;
                        }
                    }
                }
            }

            return $this->conn->query($query);
        }
    }
}